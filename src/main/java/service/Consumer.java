package service;

import command.ICommand;

import java.util.concurrent.LinkedBlockingQueue;

public class Consumer implements Runnable{
    private final LinkedBlockingQueue<ICommand> commandsQueue;

    public Consumer(LinkedBlockingQueue<ICommand> commandsQueue) {
        this.commandsQueue = commandsQueue;
    }

    @Override
    public void run() {
        while(true) {
            try {
                ICommand command = this.commandsQueue.take();
                command.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
