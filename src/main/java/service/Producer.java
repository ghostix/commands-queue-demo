package service;

import command.ICommand;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class Producer implements Runnable {

    private final LinkedBlockingQueue<ICommand> commandsQueue;
    private final List<ICommand> commandsList;

    public Producer(LinkedBlockingQueue<ICommand> commandsQueue, List<ICommand> commandsList) {
        this.commandsQueue = commandsQueue;
        this.commandsList = commandsList;
    }

    @Override
    public void run() {
        this.commandsList.forEach(command -> {
            try {
                this.commandsQueue.put(command);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
