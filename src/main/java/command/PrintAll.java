package command;

import domain.User;
import repository.IUserManager;
import repository.RepositoryProvider;

import java.util.Collection;

public class PrintAll implements ICommand {

    @Override
    public void run() {
        IUserManager dataAccessService = RepositoryProvider.getAccessService();
        Collection<User> users = dataAccessService.getAll();
        if (users.isEmpty()) {
            System.out.println("##### No users in database.");
        } else {
            System.out.println("##### Users list:");
            users.forEach(u -> System.out.println(u.toString()));
        }
    }
}
