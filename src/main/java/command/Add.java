package command;

import domain.User;
import repository.IUserManager;
import repository.RepositoryProvider;

public class Add implements ICommand {
    private final User user;

    public Add(int userID, String userGUID, String userName) {
        this.user = new User(userID, userGUID, userName);
    }

    @Override
    public void run() {
        IUserManager dataAccessService = RepositoryProvider.getAccessService();
        dataAccessService.add(this.user);
        System.out.println("##### User has been created: " + this.user.toString());
    }
}
