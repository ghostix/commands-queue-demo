package command;

import domain.User;
import repository.IUserManager;
import repository.RepositoryProvider;

import java.util.Collection;

public class DeleteAll implements ICommand {

    @Override
    public void run() {
        IUserManager dataAccessService = RepositoryProvider.getAccessService();
        Collection<User> users = dataAccessService.getAll();
        users.forEach(dataAccessService::remove);
        System.out.println("##### Users have been deleted");
    }
}
