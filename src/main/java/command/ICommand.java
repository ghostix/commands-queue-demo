package command;

public interface ICommand {
    void run() throws Exception;
}
