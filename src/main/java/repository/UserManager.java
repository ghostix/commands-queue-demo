package repository;

import domain.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.RepositoryUtils;

import java.util.Collection;
import java.util.Properties;

public class UserManager implements IUserManager {
    private static final Logger LOG = LoggerFactory.getLogger(UserManager.class);

    private final SessionFactory sessionFactory;

    public UserManager(Properties properties) {
        this.sessionFactory = RepositoryUtils.getSessionFactory(properties);
    }

    @Override
    public void add(User user) {
        Transaction transaction = null;
        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("Error: ", e);
            if (transaction != null) {
                LOG.info("user data rollback, TX rollback");
                transaction.rollback();
            }
        }
    }

    @Override
    public void remove(User user) {
        Transaction transaction = null;
        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(user);
            transaction.commit();
        } catch (Exception e) {
            LOG.error("Error: ", e);
            if (transaction != null) {
                LOG.info("user data rollback, TX rollback");
                transaction.rollback();
            }
        }
    }

    @Override
    public Collection<User> getAll() {
        try (Session session = this.sessionFactory.openSession()) {
            return session.createQuery("from User", User.class).list();
        }
    }

    @Override
    public void removeAll() {
        // TODO think about truncate table
    }

    @Override
    public void close() {
        this.sessionFactory.close();
    }
}
