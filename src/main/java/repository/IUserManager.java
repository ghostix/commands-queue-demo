package repository;

import domain.User;

import java.util.Collection;

public interface IUserManager extends AutoCloseable {
    void add(User user);
    void remove(User user);
    Collection<User> getAll();
    void removeAll();
}
