package repository;

import util.RepositoryUtils;

public class RepositoryProvider {
    private static UserManager dataAccessService;

    public static IUserManager getAccessService() {
        if (dataAccessService == null) {
            dataAccessService = new UserManager(RepositoryUtils.getH2Properties());
        }
        return dataAccessService;
    }
}
