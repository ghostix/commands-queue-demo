import command.Add;
import command.DeleteAll;
import command.ICommand;
import command.PrintAll;
import service.Consumer;
import service.Producer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    public static final int CAPACITY = 20;
    public static final int N_THREADS = 10;

    public static void main(String[] args) {
        LinkedBlockingQueue<ICommand> commandsQueue = new LinkedBlockingQueue<>(CAPACITY);

        List<ICommand> commandsList = new ArrayList<>();
        commandsList.add(new Add(1, "a1", "Robert"));
        commandsList.add(new Add(2, "a2", "Martin"));
        commandsList.add(new PrintAll());
        commandsList.add(new DeleteAll());
        commandsList.add(new PrintAll());

        Producer commandsProducer = new Producer(commandsQueue, commandsList);
        Consumer commandsConsumer = new Consumer(commandsQueue);

        ExecutorService executorService = Executors.newFixedThreadPool(N_THREADS);
        executorService.execute(commandsProducer);
        executorService.execute(commandsConsumer);
        executorService.shutdown();
    }
}