package service;

import command.ICommand;
import command.PrintAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ConsumerTest {

    private static LinkedBlockingQueue<ICommand> commandsQueue;

    @BeforeAll
    static void init() throws InterruptedException {
        commandsQueue = new LinkedBlockingQueue<>();
        commandsQueue.put(new PrintAll());
        commandsQueue.put(new PrintAll());
        commandsQueue.put(new PrintAll());
    }

    @Test
    public void testConsumer() throws InterruptedException {
        Consumer commandsConsumer = new Consumer(commandsQueue);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Assertions.assertNotNull(commandsQueue);
        Assertions.assertFalse(commandsQueue.isEmpty());
        executorService.execute(commandsConsumer);
        executorService.shutdown();
        executorService.awaitTermination(3_000, TimeUnit.MILLISECONDS);
        Assertions.assertTrue(commandsQueue.isEmpty());
    }
}
