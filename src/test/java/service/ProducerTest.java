package service;

import command.Add;
import command.ICommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerTest {

    private static LinkedBlockingQueue<ICommand> commandsQueue;
    private static List<ICommand> commandsList;

    @BeforeAll
    static void init() {
        commandsQueue = new LinkedBlockingQueue<>();
        commandsList = new ArrayList<>();
        commandsList.add(new Add(1, "test1GUID", "test1Name"));
        commandsList.add(new Add(2, "test2GUID", "test2Name"));
    }

    @Test
    public void testProducer() {
        Producer commandsProducer = new Producer(commandsQueue, commandsList);
        Assertions.assertTrue(commandsQueue.isEmpty());
        commandsProducer.run();
        Assertions.assertEquals(commandsQueue.size(), commandsList.size());
    }
}
