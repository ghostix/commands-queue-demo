package repository;

import domain.User;
import org.hibernate.cfg.Environment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Properties;

public class RepositoryTest {
    private static UserManager dataAccessService;

    public static Properties getTestProperties() {
        Properties properties = new Properties();
        properties.put(Environment.DRIVER, "org.h2.Driver");
        properties.put(Environment.URL, "jdbc:h2:mem:test");
        properties.put(Environment.USER, "sa");
        properties.put(Environment.PASS, "");
        properties.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
        properties.put(Environment.SHOW_SQL, "true");
        properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        properties.put(Environment.HBM2DDL_AUTO, "update");
        return properties;
    }

    @BeforeAll
    static void init() {
        dataAccessService = new UserManager(getTestProperties());
    }

    @Test
    public void testDataAccessService() {
        User user = new User(1, "testGUID", "testNAME");
        dataAccessService.add(user);

        Collection<User> users = dataAccessService.getAll();
        Assertions.assertNotNull(users);
        Assertions.assertFalse(users.isEmpty());
        User userDataFromDb = users.iterator().next();
        Assertions.assertNotNull(userDataFromDb);
        Assertions.assertEquals(userDataFromDb.getGuid(), "testGUID");
        Assertions.assertEquals(userDataFromDb.getName(), "testNAME");

        dataAccessService.remove(userDataFromDb);
        users = dataAccessService.getAll();
        Assertions.assertNotNull(users);
        Assertions.assertTrue(users.isEmpty());
    }

    @AfterAll
    static void shutdown() {
        dataAccessService.close();
    }
}
