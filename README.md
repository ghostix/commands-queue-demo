# Producer Consumer Solution using LinkedBlockingQueue
Create program in Java language that will process commands from FIFO queue using Producer – Consumer pattern.
Supported commands are the following:
- Add  - adds a user into a database
- PrintAll – prints all users into standard output
- DeleteAll – deletes all users from database

## Run Unit tests
```
./mwn test
```

## Run application
```
./mvnw clean install -DskipTests
./mvnw exec:java -Dexec.mainClass="Main" 
```
